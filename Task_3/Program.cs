﻿using System;
using static System.Console;

namespace Task_3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            SetWindowSize(45, 10);
            int argument = 2;

            var polynomialA = new Polynomial(4, 5, 4, 6, 4);
            var polynomialB = new Polynomial(4, 2, 3, 3, 2);

            WriteLine("Polynomial A: " + polynomialA);
            WriteLine("Polynomial B: " + polynomialB);

            WriteLine(new string('-', 50));

            var polynomialC = polynomialA + polynomialB;
            WriteLine("C = A + B: \t" + polynomialC);

            polynomialC = polynomialA - polynomialB;
            WriteLine("C = A - B: \t" + polynomialC);

            polynomialC = polynomialA * polynomialB;
            WriteLine("C = A * B: \t" + polynomialC);

            WriteLine(new string('-', 50));

            WriteLine($"Calculated polynomial C where (X={argument}): " + polynomialC.Сalculate(argument));

            ReadKey();
        }
    }
}
