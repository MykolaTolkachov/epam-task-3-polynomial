﻿using System;
using static System.Math;

namespace Task_3
{
    public class Polynomial
    {
        private readonly int power;
        private readonly double[] coefficients;

        public Polynomial(int power, params double[] coefficients)
        {
            if ((coefficients == null) || (coefficients[0] == 0))
            {
                throw new ArgumentException();
            }

            this.coefficients = coefficients;
            this.power = power;
        }

        public static Polynomial operator +(Polynomial left, Polynomial right)
        {
            if ((left == null) || (right == null))
            {
                throw new ArgumentException();
            }

            var returnValuePower = Max(left.power, right.power);
            var minPower = Min(left.power, right.power);
            var returnValueCoeffSize = Max(left.coefficients.Length, right.coefficients.Length);
            var returnValueCoefficients = new double[returnValueCoeffSize];
            returnValueCoefficients[0] = 1;

            var returnValue = new Polynomial(returnValuePower, returnValueCoefficients);

            for (int i = 0; i < minPower; i++)
            {
                returnValue.coefficients[i] = left.coefficients[i] + right.coefficients[i];
            }

            return returnValue;
        }

        public static Polynomial operator -(Polynomial left, Polynomial right)
        {
            if ((left == null) || (right == null))
            {
                throw new ArgumentException();
            }

            var returnValuePower = Max(left.power, right.power);
            var minPower = Min(left.power, right.power);
            var returnValueCoeffSize = Max(left.coefficients.Length, right.coefficients.Length);
            var returnValueCoefficients = new double[returnValueCoeffSize];
            returnValueCoefficients[0] = 1;

            var returnValue = new Polynomial(returnValuePower, returnValueCoefficients);

            for (int i = 0; i < minPower; i++)
            {
                returnValue.coefficients[i] = left.coefficients[i] - right.coefficients[i];
            }

            return returnValue;
        }

        public static Polynomial operator *(Polynomial left, Polynomial right)
        {
            if ((left == null) || (right == null))
            {
                throw new ArgumentException();
            }

            var returnValuePower = Max(left.power, right.power);
            var minPower = Min(left.power, right.power);
            var returnValueCoeffSize = Max(left.coefficients.Length, right.coefficients.Length);
            var returnValueCoefficients = new double[returnValueCoeffSize];
            returnValueCoefficients[0] = 1;

            var returnValue = new Polynomial(returnValuePower, returnValueCoefficients);

            for (int i = 0; i < minPower; i++)
            {
                returnValue.coefficients[i] = left.coefficients[i] * right.coefficients[i];
            }

            return returnValue;
        }

        public double Сalculate(double argument)
        {
            if (argument == 0.0)
            {
                throw new ArgumentException();
            }

            double resultValue = 0;
            int startPoint = 0;

            for (int i = power; i > 0; i--)
            {
                if (startPoint < coefficients.Length)
                {
                    resultValue += Pow(argument, i) * coefficients[startPoint];
                    startPoint++;
                }
            }

            return resultValue;
        }

        public override string ToString()
        {
            var tempPower = power;
            string returnValue = "";

            for (int i = 0; i < coefficients.Length - 1; i++)
            {
                if ((tempPower > 1) && (coefficients.Length > 0))
                {
                    tempPower--;

                    if (coefficients[i] == 1)
                    {
                        if (coefficients[i + 1] < 0)
                            returnValue += "X" + "^" + (power - i) + " - ";

                        else if (coefficients[i + 1] > 0)
                            returnValue += "X" + "^" + (power - i) + " + ";

                        else if (coefficients[i + 1] == 0)
                            returnValue += "X" + "^" + (power - i) + " + ";
                    }

                    else if (coefficients[i] != 0)
                    {
                        if (coefficients[i + 1] < 0)
                            returnValue += Abs(coefficients[i]) + "X" + "^" + (power - i) + " - ";

                        else if (coefficients[i + 1] > 0)
                            returnValue += Abs(coefficients[i]) + "X" + "^" + (power - i) + " + ";

                        else if (coefficients[i + 1] == 0)
                            returnValue += Abs(coefficients[i]) + "X" + "^" + (power - i) + " + ";
                    }
                }
            }

            if (tempPower == 1)
                returnValue += coefficients[coefficients.Length - 1] + "X";

            else if ((tempPower <= coefficients.Length) && (tempPower != 1))
                returnValue += coefficients[coefficients.Length - 1] + "X" + "^" + (power - coefficients.Length + 1);

            else
            {
                returnValue += coefficients[coefficients.Length - 1];
                returnValue += "X" + "^" + (power - coefficients.Length);
            }

            return returnValue;
        }
    }
}
